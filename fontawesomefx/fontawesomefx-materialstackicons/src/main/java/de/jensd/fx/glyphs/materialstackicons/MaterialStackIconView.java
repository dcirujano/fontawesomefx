package de.jensd.fx.glyphs.materialstackicons;

import de.jensd.fx.glyphs.GlyphStackIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;

public class MaterialStackIconView extends GlyphStackIcon<MaterialIconView, MaterialIcon> {

    public MaterialStackIconView() {
        super(MaterialIcon.class);
    }

    public MaterialStackIconView(double size) {
        super(size, MaterialIcon.class);
    }

    @Override
    protected MaterialIconView getGlyph(MaterialIcon glyphIcons, double size) {
        MaterialIconView materialIconView = new MaterialIconView(MaterialIcon.class.cast(glyphIcons));
        materialIconView.setGlyphSize(size);
        return materialIconView;
    }

}
